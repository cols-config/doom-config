;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Location of third-party elisp.
(add-to-list 'load-path  (concat doom-user-dir "elisp/"))

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Colin Noel Bell"
      user-mail-address "col@baibell.org")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; Integer font size is pixels, floating point (12.0) is points.
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)

;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
(setq doom-font (font-spec :family "IBM Plex Mono" :size 10.0)
      doom-variable-pitch-font (font-spec :family "IBM Plex Sans" :size 12.0)
      doom-serif-font (font-spec :family "IBM Plex Serif" :size 11.0))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:

(defvar cnb-themes '(doom-homage-black doom-homage-white) "Two element list of themes to toggle between")

(defun cnb-toggle-themes ()
  "Toggle between the two themes in `cnb-themes'."
  (interactive)
  (let ((a (car cnb-themes))
        (b (cadr cnb-themes))
        (cur_theme (car custom-enabled-themes)))
    (disable-theme cur_theme)
    (if (eq cur_theme a)
        (load-theme b)
      (load-theme a))))

(map! "<S-f5>" #'cnb-toggle-themes)

(setq modus-themes-bold-constructs t
      modus-themes-italic-constructs t
      modus-themes-mixed-fonts t
      modus-themes-to-toggle '(modus-operandi modus-vivendi)
      doom-theme 'modus-operandi) ;; Light
;; doom-theme 'doom-homage-black)

(map! "<f5>" #'modus-themes-toggle)

(setq cnb-images-dir (concat doom-user-dir "images/"))
;; (setq fancy-splash-image (concat cnb-images-dir "Nuvola_apps_emacs_vector.svg"))
(setq fancy-splash-image (concat cnb-images-dir "black-hole.png"))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
;; (setq display-line-numbers-type 'relative)
(setq display-line-numbers-type 'visual) ; works better with code folding etc.

;; When using Emacs on-line help the active point isn’t moved to the new buffer, this fixes that.
(setq help-window-select t)

;; Move deleted files to system trash.
(setq delete-by-moving-to-trash t)

;; Stretch cursor to the Glyph. I.E. If its over a tab it will be very wide.
(setq x-stretch-cursor t)

;; Do not create lock files for files being edited. Amongst other things this
;; fixes problems with code reloading not working in Elixir/Phoenix.
(setq create-lockfiles nil)

;; When saving a script make it executable.
(add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p)

;; Move mouse cursor away from text cursor.
(mouse-avoidance-mode 'exile)

;; Default dictionary for spell checker.
(setq ispell-dictionary "australian")

;; Run Node in development mode
(setenv "NODE_ENV" "development")

(setq cnb-projects-path '(("~/src" . 2) ("~/folio" . 2)))

(setq tab-always-indent t)

;;;
;;; Window configuration
;;;

;; Ctrl-l sequence
(setq recenter-positions '(top middle bottom))

;; Take new window space from all other windows (not just current).
(setq window-combination-resize t)

;; Prompt for project buffer on split window and follow.
(defadvice! prompt-for-buffer (&rest _)
  :after #'(+evil/window-split-and-follow +evil/window-vsplit-and-follow)
  (+vertico/switch-workspace-buffer))

;;;
;;; Frame configuration
;;;

;; Use the project name and buffer name as Frame title.
(setq frame-title-format
      '(""
        "%b"
        (:eval
         (let ((project-name (projectile-project-name)))
           (unless (string= "-" project-name)
             (format " in [%s]" project-name))))))

;; (after! doom-ui
;;   (if (display-graphic-p)
;;       (progn
;;         ;; (menu-bar-mode 1)
;;         (set-frame-parameter nil 'fullscreen 'fullboth))))
;; (toggle-frame-fullscreen))))

;; Transparent Frames.
;; (set-frame-parameter (selected-frame) 'alpha '(92 . 90))
;; (add-to-list 'default-frame-alist '(alpha . (92 . 90)))

;;;
;;; Initalize ASDF.
;;;

(require 'asdf)
(asdf-enable)

;;;
;;; Show info about HTTP and media types.
;;;

(use-package! know-your-http-well
  :commands (http-header http-method http-relation http-status-code media-type))

;;;
;;; info config.
;;;

(use-package! info-colors
  :commands (info-colors-fontify-node)
  :init (add-hook 'Info-selection-hook 'info-colors-fontify-node))

;;;
;;; Read ePub files
;;;
(use-package! nov
  :init
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))

;;;
;;; Better scrolling when images etc. in buffer.
;;;

;; Only in Emacs 29,
(use-package! pixel-scroll-precision-mode
  :if (locate-library "pixel-scroll-precision-mode")
  :ensure nil
  :init (pixel-scroll-precision-mode)
  :custom (pixel-scroll-precision-interpolate-page t))

;;;
;;; Whichkey
;;;

;; Hide 'evil-' and 'evilem-' in function names in whichkey menus so we can see
;; the important part of the function name in the menu.
(after! which-key
  (pushnew!
   which-key-replacement-alist
   '(("" . "\\`+?evil[-:]?\\(?:a-\\)?\\(.*\\)") . (nil . "◂\\1"))
   '(("\\`g s" . "\\`evilem--?motion-\\(.*\\)") . (nil . "◃\\1"))))

;; Show which key menu in centre of screen.
(use-package! which-key-posframe
  :config
  (which-key-posframe-mode)
  (setq which-key-posframe-parameters
        '((left-fringe . 8)
          (right-fringe . 8))))

;;;
;;; Whitespace
;;;

(after! whitespace
  ;; Show whitespace for tabs and trailing whitespace only.
  (setq whitespace-style '(face trailing tabs indentation::tab))

  ;; Set shortcut for cleaning up white-space.
  (map! :ni "M-m" #'cycle-spacing))

;;;
;;; Completion
;;;

;; Completion annotations.
(after! marginalia
  (setq marginalia-max-relative-age (* 60 60 24 7)) ; 7 day
  (setq marginalia-align 'center))

;; Completion interface.
(after! vertico
  ;; Display completion candidates in a buffer not the mini-buffer.
  ;; (vertico-buffer-mode)

  ;; Once https://github.com/doomemacs/doomemacs/issues/6296 is fixed I
  ;; should be able to remove these lines.
  (setq which-key-use-C-h-commands t)
  (map! :leader "w C-h" nil)

  ;; Don't cycle, nice to know when at end.
  (setq vertico-cycle nil)

  ;; Set a bit of space inside border.
  (setq vertico-posframe-parameters
        '((left-fringe . 8)
          (right-fringe . 8)))

  ;; (setq vertico-buffer-display-action '(display-buffer-in-direction
  ;;                                       (window-height . ,(+ 3 vertico-count))
  ;;                                       (direction . below)))
  ;; (setq vertico-buffer-display-action '(display-buffer-in-side-window
  ;;                                       (window-height . ,(+ 3 vertico-count))
  ;;                                       (side . bottom)))

  ;; Prefix current candidate with "» ".
  (advice-add #'vertico--format-candidate :around
              (lambda (orig cand prefix suffix index start)
                (setq cand (funcall orig cand prefix suffix index start))
                (concat
                 (if (= vertico--index index)
                     (propertize "» " 'face 'vertico-current)
                   "  ")
                 cand))))

;;;
;;; Shell mode
;;;

(after! sh-script
  (setq sh-basic-offset 2))

;;;
;;; Treemacs
;;;

(use-package! treemacs
  :init
  (setq +treemacs-git-mode 'deferred))

;;;
;;; dired
;;;

(use-package! casual-dired
  :bind (:map dired-mode-map ("<f12>" . 'casual-dired-tmenu)))

;;;
;;; calc
;;;

(use-package casual
  :ensure t
  :bind (:map calc-mode-map ("<f12>" . 'casual-main-menu)))

;;;
;;; Ruby Mode
;;;

;; Use bundler for ruby lsp
(after! ruby
  (setq lsp-solargraph-use-bundler t))

;;;
;;; Elixir Mode
;;;

(after! elixir-mode
  (setq flycheck-elixir-credo-strict t)

  ;; Treat exunit test results, format error and mix compile windows as popups.
  (set-popup-rule! "^\\*exunit-compilation\\*" :size 0.25)
  (set-popup-rule! "^\\*elixir-format-errors\\*" :size 0.25)
  (set-popup-rule! "^\\*mix compile\\*" :size 0.25)

  ;; Wrap exunit error messages
  (add-hook 'exunit-compilation-mode-hook 'turn-on-visual-line-mode)

  (use-package! mix
    :config
    (add-hook 'elixir-mode-hook 'mix-minor-mode)
    (map! :localleader
          :map elixir-mode-map
          :prefix ("m" . "mix")
          "c" #'mix-compile
          "l" #'mix-last-command
          "m" #'mix-execute-task)))

;; (use-package polymode
;;   :mode ("\.ex$" . poly-elixir-web-mode)
;;   :config
;;   (define-hostmode poly-elixir-hostmode :mode 'elixir-mode)
;;   (define-innermode poly-liveview-expr-elixir-innermode
;;     :mode 'web-mode
;;     :head-matcher (rx line-start (* space) "~H" (= 3 (char "\"'")) line-end)
;;     :tail-matcher (rx line-start (* space) (= 3 (char "\"'")) line-end)
;;     :head-mode 'host
;;     :tail-mode 'host
;;     :allow-nested nil
;;     :keep-in-mode 'host
;;     :fallback-mode 'host)
;;   (define-polymode poly-elixir-web-mode
;;     :hostmode 'poly-elixir-hostmode
;;     :innermodes '(poly-liveview-expr-elixir-innermode)))

;; (setq web-mode-engines-alist '(("elixir" . "\\.ex\\'")))


;;;
;;; Haskell Mode
;;;

(after! haskell-mode
  (setq haskell-interactive-popup-errors nil)  )

;;;
;;; vimrc Mode
;;;

(use-package! vimrc-mode
  :init (add-to-list 'auto-mode-alist '("\\.vim\\(rc\\)?\\'" . vimrc-mode)))

;;;
;;; JS Mode
;;;

;; (after! js2-mode (setq js2-basic-offset 2))

(after! javascript
  (setq javascript-indentation 2
        js-indent-level 2))

;;;
;;; Web Mode
;;;

;; Set web-mode indentation
(after! web
  (setq web-mode-markup-indent-offset 2
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2))

;;;
;;; CSS etc.
;;;

;; Overwrite existing scss-stylelint checker to not use --syntax
;; See https://github.com/flycheck/flycheck/issues/1912
(flycheck-define-checker scss-stylelint
  "A SCSS syntax and style checker using stylelint.

See URL `http://stylelint.io/'."
  :command ("stylelint"
            (eval flycheck-stylelint-args)
            ;; "--syntax" "scss"
            (option-flag "--quiet" flycheck-stylelint-quiet)
            (config-file "--config" flycheck-stylelintrc))
  :standard-input t
  :error-parser flycheck-parse-stylelint
  :modes (scss-mode))

;; scss flycheck checker is in node modules directory
(after! scss
  ;; (eval-after-load 'scss-mode
  (add-hook 'scss-mode-hook #'add-node-modules-path))

;;;
;;; Magit
;;;

(after! magit
  (setq magit-repository-directories cnb-projects-path)

  ;; Don't want TODOs from third-parties showing up.
  (add-to-list 'magit-todos-exclude-globs "assets/vendor/")

  ;; Show gravatars.
  (setq magit-revision-show-gravatars '("^Author:     " . "^Commit:     "))

  ;; Show magit in full frame.
  ;; (setq magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1)

  ;; Lets not wait for large Magit diffs, I.E. large merges.
  (setq magit-diff-expansion-threshold 5))

(use-package magit-file-icons
  :ensure t
  :init
  (magit-file-icons-mode 1)
  :custom
  ;; These are the default values:
  (magit-file-icons-enable-diff-file-section-icons t)
  (magit-file-icons-enable-untracked-icons t)

  (magit-file-icons-enable-diffstat-icons t))

;;;
;;; LSP
;;;

(after! lsp-mode
  (setq lsp-lens-enable t
        lsp-headerline-breadcrumb-enable t)

  ;; Ignore some directories in LSP
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]cover\\'")
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]deps\\'")
  (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]Mnesia.nonode@nohost\\'"))

(after! lsp-ui
  (setq lsp-ui-sideline-show-hover t
        lsp-ui-imenu-auto-refresh t
        lsp-ui-sideline-show-code-actions t
        ;; lsp-ui-sideline-show-symbol nil
        ;; lsp-ui-sideline-show-diagnostics nil
        lsp-ui-doc-position 'top
        ;; lsp-ui-doc-position 'at-point
        lsp-ui-doc-show-with-cursor nil))

;;;
;;; Modeline
;;;

(setq doom-modeline-icon (display-graphic-p)
      doom-modeline-major-mode-icon t
      doom-modeline-major-mode-color-icon t
      doom-modeline-indent-info t)

(unless (string-match-p "^Power N/A" (battery))   ; On laptops...
  (display-battery-mode 1))                       ; it's nice to know how much power you have

;;;
;;; Project management
;;;

;; Create new workspaces when switching projects.
(setq +workspaces-on-switch-project-behavior t)

(after! projectile
  (setq projectile-project-search-path cnb-projects-path))

;; (map! :map doom-leader-project-map "b" #'consult-project-buffer)

;; (after! persp
;;   (setq-hook! 'persp-mode-hook uniquify-buffer-name-style 'forward)) ; Unique buffer names. This may cause problems. see https://github.com/doomemacs/doomemacs/issues/6205

;; Keep a TODOS file for each project.
;; (use-package! org-projectile
;;   :after (org)
;;   :config
;;   (progn
;;     (org-projectile-per-project)
;;     (setq org-projectile-per-project-filepath "TODO.org")
;;     (setq org-agenda-files (append org-agenda-files (org-projectile-todo-files)))
;;     (push (org-projectile-project-todo-entry) org-capture-templates)
;;     (push (org-projectile-project-todo-entry
;;            :capture-template "* TODO %? %A\n"
;;            :capture-heading "Create linked org-projectile TODO" :capture-character "l")
;;           org-capture-templates)
;;     (push (org-projectile-project-todo-entry) org-capture-templates)
;;     (map! :leader
;;           (:prefix "n"
;;            :desc "Project TODOs" "p" #'org-projectile-project-todo-completing-read))))

;;
;; PlantUML
;;

(after! plantuml
  (setq plantuml-indent-level 2))

;;;
;;; ORG
;;;

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org")

(use-package! org
  :defer

  :config
  (setq org-src-window-setup 'reorganize-frame)
  (setq org-startup-folded t)
  (setq org-agenda-inhibit-startup nil)

  ;; Org - Allow tab to recursively toggle subtree
  (after! evil-org
    (remove-hook 'org-tab-first-hook #'+org-cycle-only-current-subtree-h)))

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
